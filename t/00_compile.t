use strict;
use warnings;
use Test::More;


use AmonTen;
use AmonTen::Web;
use AmonTen::Web::View;
use AmonTen::Web::ViewFunctions;

use AmonTen::DB::Schema;
use AmonTen::Web::Dispatcher;

use AmonTen::Model::RandomQuote;
use AmonTen::Model::MoonPhase;
use AmonTen::Model::RandomNumber;

pass "All modules can load.";

done_testing;
