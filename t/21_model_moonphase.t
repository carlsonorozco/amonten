use strict;
use warnings;
use Test::More;

use AmonTen::Model::MoonPhase;

my $date = "2013-10-22";

my $mp = AmonTen::Model::MoonPhase->new;

note( $mp->phase($date) );

ok( $mp->phase($date), "got moon phase for $date" );

is( $mp->illumination($date), [ $mp->phase($date) ]->[1],
    "got illumination" );

is( $mp->age($date), [ $mp->phase($date) ]->[2], "got age" );

note( $mp->is_waxing($date) );
is( $mp->is_waxing($date), '', "got is_waxing = ''" );
is( $mp->is_waning($date), 1,  "got is_waning = 1" );

is( $mp->is_gibbous($date), 1, "got is_gibbous = 1" );

done_testing;
