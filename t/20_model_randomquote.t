use strict;
use warnings;

use Test::More;

use AmonTen::Model::RandomQuote;

ok( AmonTen::Model::RandomQuote::get_one() );
ok( AmonTen::Model::RandomQuote::get_all() );

done_testing;
