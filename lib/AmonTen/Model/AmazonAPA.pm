package AmonTen::Model::AmazonAPA;
use strict;
use warnings;

use URI::Amazon::APA;
use Furl;
use XML::LibXML;
use XML::LibXML::XPathContext;
use Moose;
use Carp;

has [ 'key_id', 'secret' ] => ( is => 'ro', required => 1 );

sub covers_for_title {
    my $self = shift;

    my $title = shift or die "You must provide a title string";

    my $uri = URI::Amazon::APA->new('http://webservices.amazon.com/onca/xml');
    $uri->query_form(
        Service       => 'AWSEcommerceService',
        Operation     => 'ItemSearch',
        Title         => $title,
        SearchIndex   => 'Books',
        AssociateTag  => 'apv-20',
        ResponseGroup => 'Large',
    );
    $uri->sign(
        key    => $self->key_id,
        secret => $self->secret,
    );

    my $r = Furl->new->get($uri);

    my $doc = XML::LibXML->new->parse_string( $r->content );
    my $xc  = XML::LibXML::XPathContext->new($doc);
    $xc->registerNs( 'amzn', $doc->getDocumentElement->namespaceURI );

    if ( $r->is_success ) {

        return
            map { $_->textContent }
            $xc->findnodes('//amzn:MediumImage/amzn:URL');
    }
    elsif ( my @err = $xc->findnodes('//amzn:Error') ) {
        my $err = join '', map { $_->textContent } @err;
        die $err, "\n";
    }
    else {
        croak "Unexpected error in ", __PACKAGE__, " request to ",
            $uri->as_string;
    }
}

1;
