package AmonTen::Model::RandomNumber;
use strict;
use warnings;
use Carp;
use Moose;
use Furl;
use URI;

sub generate {
    my ( $self, $args ) = @_;
    $args ||= {};
    ref($args) eq 'HASH' or croak "Arguments must be a hash reference";

    my $upper = delete $args->{upper_bound};
    my $lower = delete $args->{lower_bound} if $upper;

    # Both or neither.
    my $integer = delete $args->{integer} if $upper;

    # Only works if bounds are set.
    croak "Unsupported args or combination of args", join( ", ", %{$args} )
        if keys %{$args};

    # Reasonable defaults if not set by client.
    $upper ||= 1;
    $lower ||= 0;

    $upper > $lower
        or croak "upper_bound is not greater than lower_bound";

    if ($integer) {
        _generate_int( $lower, $upper );
    }
    else {
        rand( $upper - $lower ) + $lower;
    }
}

sub _generate_int {
    my ( $min, $max ) = @_;

    my $u = URI->new('http://www.random.org/integers/');
    $u->query_form(
        num    => 1,
        min    => $min,
        max    => $max,
        base   => 10,
        col    => 1,
        format => 'plain',
        rnd    => 'new',
    );
    my $r = Furl->new->get($u);
    $r->is_success or croak( join "\n", $u, $r->status_line );
    chomp( my $n = $r->content );
    $n;
}

1;
