package AmonTen::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;

any '/' => sub {
    my ($c) = @_;
    return $c->render('index.tx');
};

post '/account/logout' => sub {
    my ($c) = @_;
    $c->session->expire();
    return $c->redirect('/');
};

# /randomquote

use AmonTen::Model::RandomQuote;

get '/randomquote/get_one' => sub {
    my ($c) = @_;

    my $q = AmonTen::Model::RandomQuote::get_one();

    $c->render( 'views/randomquote.tx', { quote => $q } );
};

get '/randomquote/get_all' => sub {
    my ($c) = @_;

    my $q = AmonTen::Model::RandomQuote::get_all();

    $c->render( 'views/randomquote.tx', { quote_all => $q } );
};

# /moonphase

use AmonTen::Model::MoonPhase;

get '/moonphase' => sub {
    my $c = shift;
    $c->render('views/moonphase_form.tx');
};

post '/moonphase' => sub {
    my $c = shift;
    my $date = $c->req->param('date');
    $c->redirect('/moonphase' . ($date ? "/$date" : "") ) ;
};

get '/moonphase/:date' => sub {
    my ( $c, $args ) = @_;
    my $date = $args->{date};
    my $mp   = AmonTen::Model::MoonPhase->new;

    $c->render(
        'views/moonphase.tx',
        {   when => $date,
            illum_percent =>
                sprintf( "%.2f", $mp->illumination($date) * 100 ),
            is_waxing => $mp->is_waxing($date),
        }
    );
};

# /covers

use AmonTen::Model::AmazonAPA;

get '/covers/:title' => sub {
    my ( $c, $args ) = @_;
    my $title = $args->{title};

    my $apa = AmonTen::Model::AmazonAPA->new(
        key_id => $c->config->{AWS}->{access_key_id},
        secret => $c->config->{AWS}->{access_key_secret},
    );
    my @covers = $apa->covers_for_title($title);

    $c->render( 'views/covers.tx', { title => $title, covers => \@covers } );
};

# /randomnumber

use AmonTen::Model::RandomNumber;

get '/randomnumber' => sub {
    my ($c) = @_;

    my $g = AmonTen::Model::RandomNumber->new;
    $c->create_response( 200, [], [ $g->generate ] );
};

get '/randomnumber/d6' => sub {
    my ($c) = @_;

    my $g = AmonTen::Model::RandomNumber->new;
    $c->create_response(
        200,
        [],
        [   $g->generate(
                { lower_bound => 1, upper_bound => 6, integer => 1 }
            )
        ]
    );
};

# /stock

use AmonTen::Model::StockQuote;

get '/stock' => sub {
    my ($c) = @_;
    $c->render('views/stock.tx');
};

any '/stock/ajax' => sub {
    my ($c) = @_;

    my $q = AmonTen::Model::StockQuote->new;

    my @stocks = $c->req->param('stocks') =~ /(\w+)/g;
    my %raw_quotes = $q->fetch( 'asia', @stocks );
    my $quotes = _normalize_hash(%raw_quotes);
    $c->render_json({ results => [ values %{$quotes} ] });
};

sub _normalize_hash {
    my %funky_hash = @_;
    my %hash;
    for my $compound_key ( keys %funky_hash ) {
        my ( $name, $key ) = split /$;/, $compound_key;
        $hash{$name}{$key} = $funky_hash{$compound_key};
    }
    return \%hash;
}

1;
