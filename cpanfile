requires 'Amon2', '5.10';
requires 'DBD::SQLite', '1.33';
requires 'HTML::FillInForm::Lite', '1.11';
requires 'JSON', '2.50';
requires 'Module::Functions', '2';
requires 'Plack::Middleware::ReverseProxy', '0.09';
requires 'Plack::Middleware::Session';
requires 'Plack::Session', '0.14';
requires 'Router::Boom', '0.06';
requires 'Starlet', '0.20';
requires 'Teng', '0.18';
requires 'Test::WWW::Mechanize::PSGI';
requires 'Text::Xslate', '2.0009';
requires 'Time::Piece', '1.20';
requires 'perl', '5.010_001';

requires 'Moose', '2.1005';

requires 'Astro::MoonPhase', '0.60';
requires 'Date::Manip', '6.41';

requires 'URI::Amazon::APA', '0.05';
requires 'Furl', '2.19';
requires 'XML::LibXML', '2.0106';

requires 'Finance::Quote', '1.18';

on configure => sub {
    requires 'Module::Build', '0.38';
    requires 'Module::CPANfile', '0.9010';
};

on test => sub {
    requires 'Test::More', '0.98';
};
